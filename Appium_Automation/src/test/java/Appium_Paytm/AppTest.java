package Appium_Paytm;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;

public class AppTest {
	
	public static AppiumDriverLocalService service;
	public static AppiumServiceBuilder builder;
	
	
	@Test
	public void test() throws IOException {
		
		service =  AppiumDriverLocalService.buildDefaultService();
		service.start();
		
		File f = new File("src/paytm.apk");
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Android");
		cap.setCapability(MobileCapabilityType.APP, f.getAbsolutePath());
		AndroidDriver<AndroidElement> driver = new AndroidDriver<AndroidElement>(new URL("http://127.0.0.1:4723/wd/hub"),cap);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(By
		        .xpath("//android.widget.Button[@resource-id='net.one97.paytm:id/btn_language_continue_button']")));
		driver.findElementByXPath("*//android.widget.Button[@resource-id='net.one97.paytm:id/btn_language_continue_button']").click();
		for (int i=0;i<3;i++) {
			driver.findElementByXPath("*//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']").click();
		}
		driver.pressKeyCode(AndroidKeyCode.BACK);
		driver.findElementByXPath("*//android.widget.Button[@resource-id='com.android.packageinstaller:id/permission_allow_button']").click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		service.stop();
		
	}
	
}